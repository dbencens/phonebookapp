package com.example.dan.phonebookapp_github;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    EditText labelTxt, numberTxt, emailTxt, addressTxt;
    List<ContactView> Contact = new ArrayList<ContactView>();
    ListView contactView;
    int longClick;
    ArrayAdapter<ContactView> contactAdap;
    DatabaseHandler databasehandler;
    private static final int DELETE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//declaring variables
        labelTxt = (EditText) findViewById(R.id.Labeltxt);
        numberTxt = (EditText) findViewById(R.id.Numbertxt);
        emailTxt = (EditText) findViewById(R.id.Emailtxt);
        addressTxt = (EditText) findViewById(R.id.Addresstxt);
        contactView = (ListView) findViewById(R.id.listView);
        databasehandler = new DatabaseHandler(getApplicationContext());
        final Button addBtn = (Button) findViewById(R.id.btnadd);
//setting up the tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);

        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("AddContact");
        tabSpec.setContent(R.id.tabAdd);
        tabSpec.setIndicator("Add");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("ViewContacts");
        tabSpec.setContent(R.id.tabView);
        tabSpec.setIndicator("Contacts");
        tabHost.addTab(tabSpec);
// long click
        registerForContextMenu(contactView);
        contactView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                longClick = position;

                return false;
            }
        });
//This is what will happen when the button is clicked, this adds a new contact to the database
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactView contact = new ContactView(databasehandler.getCount(), String.valueOf(labelTxt.getText()), String.valueOf(numberTxt.getText()), String.valueOf(emailTxt.getText()), String.valueOf(addressTxt.getText()));
                if (contactExists(contact)) {

                    Toast.makeText(getApplicationContext(), " This name already exists", Toast.LENGTH_SHORT).show();
                    //if contact already exists it will not add it again and display a warning
                }else{
                    databasehandler.createContact(contact);

                    contactAdap.notifyDataSetChanged();
                    Contact.add(contact);
                    databasehandler.getAllContacts();
                    Toast.makeText(getApplicationContext(), "Contact Added", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), String.valueOf(labelTxt.getText()) + " GoT ReKT", Toast.LENGTH_SHORT).show();

                    return;

                }
            }});

        labelTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                addBtn.setEnabled(String.valueOf(labelTxt.getText()).trim().length() > 0);}

            @Override
            public void afterTextChanged(Editable editable) {}
        });

//if contact count is not = to 0 it adds all the contacts and repopulates the list
        if (databasehandler.getCount() != 0)
            Contact.addAll(databasehandler.getAllContacts());
        populateList();
    }
    //remove user on long click setup
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo info){
        super.onCreateContextMenu(menu, view, info);
        menu.setHeaderTitle("Remove User");
        menu.add(menu.NONE, DELETE, menu.NONE, "Delete");
    }
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case DELETE:
                databasehandler.delContact(Contact.get(longClick));
                Contact.remove(longClick);
                contactAdap.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
                break; }
        return super.onContextItemSelected(item);
    }
    private boolean contactExists(ContactView contact) {
        String name = contact.getLabel();
        int contactCount = Contact.size();

        for (int i = 0; i < contactCount; i++) {
            if (name.compareToIgnoreCase(Contact.get(i).getLabel()) == 0)
                return true;
        }
        return false;
    }

    private void populateList() {
        contactAdap = new ContactListAdapter();
        contactView.setAdapter(contactAdap);
    }

    private class ContactListAdapter extends ArrayAdapter<ContactView> {
        public ContactListAdapter() {
            super (MainActivity.this, R.layout.list_item, Contact);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.list_item, parent, false);

            ContactView currentContact = Contact.get(position);

            TextView name = (TextView) view.findViewById(R.id.contactName);
            name.setText(currentContact.getLabel());
            TextView phone = (TextView) view.findViewById(R.id.phoneNumber);
            phone.setText(currentContact.getNumber());
            TextView email = (TextView) view.findViewById(R.id.emailAddress);
            email.setText(currentContact.getEmail());
            TextView address = (TextView) view.findViewById(R.id.contactAddres);
            address.setText(currentContact.getAddress());


            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
