package com.example.dan.phonebookapp_github;

/**
 * Created by Dan on 20/03/2015.
 */

public class ContactView {


    private String _label, _number, _email, _address;

    private int _id;

    public ContactView (int id, String label, String number, String email, String address) {
        _id = id;
        _label = label;
        _number = number;
        _email = email;
        _address = address;
    }

    public int getId() { return _id; }
    public String getLabel() {
        return _label;
    }
    public String getNumber() {
        return _number;
    }
    public String getEmail() {
        return _email;
    }
    public String getAddress() {
        return _address;
    }

}