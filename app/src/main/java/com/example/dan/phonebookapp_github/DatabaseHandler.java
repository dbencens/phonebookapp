package com.example.dan.phonebookapp_github;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dan on 20/03/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    //Assigning the fields in the database
    private static final String DATABASE_NAME = "phoneBook",
            DATA_TABLE = "contacts", KEY_ID = "id", KEY_LABEL = "label", KEY_NUMBER = "number", KEY_EMAIL = "email", KEY_ADDRESS = "address";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);}
    //creates the database with the fields above
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DATA_TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_LABEL + " TEXT," + KEY_NUMBER + " TEXT," + KEY_EMAIL + " TEXT," + KEY_ADDRESS + " TEXT)");

    }
    //when updating drops the table if it exists and creates a new one
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATA_TABLE);

        onCreate(db);

    }
    //creates what will be a contact on the phonebook
    public void createContact(ContactView contact) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_LABEL, contact.getLabel());
        values.put(KEY_NUMBER, contact.getNumber());
        values.put(KEY_EMAIL, contact.getEmail());
        values.put(KEY_ADDRESS, contact.getAddress());


        db.insert(DATA_TABLE, null, values);

        db.close();
    }

    public ContactView getContact(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(DATA_TABLE, new String[] { KEY_ID, KEY_LABEL, KEY_NUMBER, KEY_EMAIL, KEY_ADDRESS }, KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null, null );

        if (cursor != null)
            cursor.moveToFirst();

        ContactView contact = new ContactView(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        //closing the cursor and database after using them to free up resources
        db.close();
        cursor.close();
        return contact;
    }
    //used when deleting a contact
    public void delContact(ContactView contact) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DATA_TABLE, KEY_ID + "=?", new String[] { String.valueOf(contact.getId()) });
        db.close();
    }

    public int getCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DATA_TABLE, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();

        return count;
    }
    //updates the affected rows
    public int updateContact(ContactView contact) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_LABEL, contact.getLabel());
        values.put(KEY_NUMBER, contact.getNumber());
        values.put(KEY_EMAIL, contact.getEmail());
        values.put(KEY_ADDRESS, contact.getAddress());

        int rowsAffected = db.update(DATA_TABLE, values, KEY_ID + "=?", new String[] { String.valueOf(contact.getId()) });

        db.close();

        return rowsAffected;
    }

    public List<ContactView> getAllContacts() {
        List<ContactView> contacts = new ArrayList<ContactView>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DATA_TABLE + " ORDER BY " + KEY_LABEL + " ASC", null, null);

        if (cursor.moveToFirst()) {
            do {
                contacts.add(new ContactView(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contacts;
    }
}